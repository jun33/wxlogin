package com.jun.core.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 自定义授权验证拦截器
 * 
 * @author jun
 *
 */
public class OAuthInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if ("GET".equalsIgnoreCase(request.getMethod())) {
			request.setCharacterEncoding("UTF-8");
			System.out.println("Get请求方式拦截，如果需要，此处修改Code");
		}
		HttpSession session = request.getSession();
		String openid = (String) session.getAttribute("openid");
		if (StringUtils.isBlank(openid)) {
			session.setAttribute("firstUrl", request.getRequestURL().toString());
			System.out.println("openid为空");
			request.getRequestDispatcher("/oauth/entrance").forward(request, response);
			return false;
		}
		return true;
	}
}
