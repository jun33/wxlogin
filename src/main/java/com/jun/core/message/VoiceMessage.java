package com.jun.core.message;

public class VoiceMessage extends BaseMessage {
	// 语音
	private String voiceUrl;

	public String getVoiceUrl() {
		return voiceUrl;
	}

	public void setVoiceUrl(String voiceUrl) {
		this.voiceUrl = voiceUrl;
	}
}
