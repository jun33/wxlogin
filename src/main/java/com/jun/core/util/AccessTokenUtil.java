package com.jun.core.util;

import com.alibaba.fastjson.JSONObject;

public class AccessTokenUtil {
	/**
	 * 获取accessToken
	 * 
	 * @param appID
	 *            微信公众号凭证
	 * @param appScret
	 *            微信公众号凭证秘钥
	 * @return
	 */
	public static AccessToken getAccessToken(String appID, String appScret) {
		AccessToken token = new AccessToken();
		// 访问微信服务器获取acess_token接口
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appID + "&secret="
				+ appScret;
		JSONObject json = OAuthUtil.doGetJson(url);
		token.setAccess_token(json.getString("access_token"));
		token.setExpires_in(new Integer(json.getString("expires_in")));
		return token;
	}
}
