package com.jun.core.util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * 启动定时获取access_token的线程
 * 
 * @author jun
 *
 */
public class GetAccessTokenServlet extends HttpServlet {
	private static final long serialVersionUID = -3220735905014023881L;

	@Override
	public void init() throws ServletException {
		new Thread(new TokenThread()).start();
	}
}
