package com.jun.core.util;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.alibaba.fastjson.JSONObject;

public class OAuthUtil {
	public static final String APPID = "wx57522773b9438002";
	public static final String APPSECRET = "6719c739f832243885c6b6c7278786b0";

	public static JSONObject doGetJson(String url) {
		JSONObject json = null;
		try {
			URL getUrl = new URL(url);
			HttpURLConnection http = (HttpURLConnection) getUrl.openConnection();
			http.setRequestMethod("GET");
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			http.setDoOutput(true);
			http.setDoInput(true);

			http.connect();
			InputStream is = http.getInputStream();
			int size = is.available();
			byte[] b = new byte[size];
			is.read(b);
			String message = new String(b, "UTF-8");
			System.out.println(message);
			json = JSONObject.parseObject(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
}
