package com.jun.core.controller;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.jun.core.util.OAuthUtil;

@Controller
@RequestMapping("oauth")
public class WxOAuthController {
	@RequestMapping("home")
	public String goHome() {
		System.out.println("Home");
		return "kk";
	}

	@RequestMapping("entrance")
	public String goFirstGet(HttpServletRequest req, HttpServletResponse resp) {
		String backUrl = "http://juncloud.imwork.net/wxChat/oauth/back";
		String oauthUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + OAuthUtil.APPID
				+ "&redirect_uri=" + URLEncoder.encode(backUrl) + "&response_type=code&scope=" + "snsapi_base"
				+ "&state=" + req.getSession().getAttribute("firstUrl") + "#wechat_redirect";
		return "redirect:" + oauthUrl;
	}

	@RequestMapping("back")
	public String goBack(HttpServletRequest req) {
		Map<String, String> pars = new HashMap<String, String>();
		pars.put("code", req.getParameter("code"));
		pars.put("firstUrl", req.getParameter("state"));
		String tokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + OAuthUtil.APPID + "&secret="
				+ OAuthUtil.APPSECRET + "&code=" + pars.get("code") + "&grant_type=authorization_code";
		JSONObject jsonResult = OAuthUtil.doGetJson(tokenUrl);
		pars.put("openid", jsonResult.getString("openid"));
		pars.put("refreshToken", jsonResult.getString("refresh_token"));
		// 会员注册验证（查表）
		// vipService.queryByOpenid(openid);
		boolean queryResult = true;
		// 如果没有，跳到注册页面
		if (!queryResult) {
			return "redirect:/register.jsp?openid=" + pars.get("openid") + "&firstUrl=" + pars.get("firstUrl");
		}
		// 刷新acess_token
		String refreshUrl = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + OAuthUtil.APPID
				+ "&grant_type=refresh_token&refresh_token=" + pars.get("refreshToken");
		OAuthUtil.doGetJson(refreshUrl);
		HttpSession session = req.getSession();
		session.setAttribute("openid", pars.get("openid"));
		/*
		 * String access_token = jsonResult.getString("access_token"); String
		 * infoUrl = " https://api.weixin.qq.com/sns/userinfo?access_token=" +
		 * access_token + "&openid=" + openid + "&lang=zh_CN"; JSONObject
		 * infoJson = OAuthUtil.doGetJson(infoUrl);
		 * System.out.println("用户openid:" + openid);
		 * System.out.println("用户nickname:" + infoJson.getString("nickname"));
		 * System.out.println("用户province:" + infoJson.getString("province"));
		 */
		// 如果数据库存在该openid，直接跳转到用户之前访问的页面
		System.out.println(pars.get("firstUrl"));
		return "redirect:" + pars.get("firstUrl");
	}
}
