package com.jun.core.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("vip")
public class TestController {
	@RequestMapping("home")
	public String goHome(HttpServletRequest req) {
		String openid = (String) req.getSession().getAttribute("openid");
		System.out.println("Home------" + openid);
		return "kk";
	}
}
